import 'package:flutter/material.dart';
import 'package:pochta/steep_floors.dart';
import 'package:pochta/widgets/button.dart';
import 'package:pochta/widgets/outline_button.dart';
import 'package:pochta/widgets/page_leading.dart';

import 'models/address.dart';
import 'models/status.dart';

class StepStatus extends StatefulWidget {
  final Address address;

  const StepStatus({Key key, this.address}) : super(key: key);

  @override
  _StepStatusState createState() => _StepStatusState();
}

class _StepStatusState extends State<StepStatus> {
  List<BuildingStatus> selectValues = [];

  @override
  void initState() {
    super.initState();
    _loadTypes();
  }

  _loadTypes() async {
    var statuses = await BuildingStatus.geetList();
    setState(() {
      selectValues = statuses;
    });
  }

  @override
  Widget build(BuildContext context) {
    var markers = selectValues.map((item) {
      return Row(
        children: [
          Radio(
            value: item.id,
            groupValue: widget.address.building_status,
            onChanged: (value) {
              setState(() {
                widget.address.building_status = value;
              });
            },
          ),
          Text(
            item.name,
            style: TextStyle(fontSize: 22),
          ),
        ],
      );
    }).toList();

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        leading: IconButton(
          icon: Icon(Icons.close, color: Colors.black),
          onPressed: () => Navigator.of(context).pop(),
        ),
        backgroundColor: Color(0xFFFFFFFF),
        elevation: 0,
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              PageLeading(step: "Шаг 3", label: "Состояние"),
              Padding(
                padding: EdgeInsets.only(bottom: 56),
              ),
              ...markers
            ],
          ),
        ),
      ),
      bottomNavigationBar: Padding(
          padding: EdgeInsets.all(12),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(right: 8.0),
                  child: PButtonOutline(
                    text: "Назад",
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(left: 8.0),
                  child: PButton(
                    text: "Далее",
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => StepFloor(
                                  address: widget.address,
                                )),
                      );
                    },
                  ),
                ),
              ),
            ],
          )),
    );
  }
}
