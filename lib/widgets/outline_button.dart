import 'package:flutter/material.dart';

class PButtonOutline extends StatelessWidget {
  final Function onPressed;
  final String text;

  const PButtonOutline({Key key, @required this.onPressed, @required this.text})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return OutlineButton(
      onPressed: () => {this.onPressed()},
      //color: Color(0xFF1A37FF),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
      borderSide: BorderSide(width: 2, color: Color(0xFF1A37FF)),
      padding: EdgeInsets.all(22),
      child: Text(
        this.text,
        style: TextStyle(color: Color(0xFF1A37FF), fontSize: 24),
      ),
    );
  }
}
