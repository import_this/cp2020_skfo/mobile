import 'package:flutter/material.dart';

class PageLeading extends StatelessWidget {
  const PageLeading({
    Key key,
    @required this.step,
    @required this.label,
  }) : super(key: key);

  final String step;
  final String label;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          this.step,
          style: TextStyle(
              color: Color(0x7F000000),
              fontWeight: FontWeight.w500,
              fontSize: 18),
        ),
        Text(this.label,
            style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.w500,
                fontSize: 28)),
      ],
    );
  }
}
