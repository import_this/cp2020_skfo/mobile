import 'package:flutter/material.dart';

class PButton extends StatelessWidget {
  final Function onPressed;
  final String text;

  const PButton({Key key, @required this.onPressed, @required this.text})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      onPressed: () => {this.onPressed()},
      color: Color(0xFF1A37FF),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
      padding: EdgeInsets.all(22),
      child: Text(
        this.text,
        style: TextStyle(color: Colors.white, fontSize: 24),
      ),
    );
  }
}
