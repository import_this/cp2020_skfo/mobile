import 'package:flutter/material.dart';

class CustomInput extends StatefulWidget {
  final String label;
  final String hint;
  final TextInputType type;
  final String storeKey;

  final Function onChanged;

  CustomInput({
    Key key,
    this.label,
    this.hint,
    this.type = TextInputType.text,
    this.storeKey = "test",
    this.onChanged,
  }) : super(key: key);

  @override
  _CustomInput createState() => _CustomInput();
}

class _CustomInput extends State<CustomInput> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 24, bottom: 24),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
              padding: const EdgeInsets.only(bottom: 12),
              child: Text(
                widget.label,
                style: TextStyle(
                    color: Color(0x7F000000),
                    fontSize: 18,
                    fontWeight: FontWeight.w500),
              )),
          TextField(
            //keyboardType: widget.type,
            textCapitalization: TextCapitalization.words,
            maxLines: 100,
            minLines: 1,
            style: TextStyle(color: Colors.black, fontSize: 22),
            onChanged: (value) => {widget.onChanged(value)},
            //controller: myController,
            decoration: InputDecoration(
              filled: true,
              fillColor: Color(0xFFF2F3F5),
              contentPadding: EdgeInsets.all(22),
              isDense: true,

              enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10.0),
                  borderSide: BorderSide(
                    color: Color.fromARGB(30, 0, 0, 0), //Color of the border
                    style: BorderStyle.solid, //Style of the border
                    width: 0.5, //width of the border
                  )),
              border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10.0),
                  borderSide: BorderSide(
                    color: Color.fromARGB(30, 0, 0, 0), //Color of the border
                    style: BorderStyle.solid, //Style of the border
                    width: 0.5, //width of the border
                  )),
              //hintText: widget.hint,
              hintStyle: TextStyle(fontSize: 16.0, color: Color(0xFF818C99)),
            ),
          ),
        ],
      ),
    );
  }
}
