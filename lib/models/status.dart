import 'dart:convert';

import 'package:http/http.dart' as http;

class BuildingStatus {
  final int id;
  final String name;

  BuildingStatus(this.id, this.name);

  static Future<List<BuildingStatus>> geetList() async {
    var response = await http.get(
        'https://api-post-info.midvikus.com/api/building_statuses/',
        headers: {'Content-Type': 'application/json; charset=utf-8'});

    List<BuildingStatus> data = [];
    for (var item in json.decode(utf8.decode(response.bodyBytes))) {
      data.add(BuildingStatus(item["id"], item["name"]));
    }
    return data;
  }
}
