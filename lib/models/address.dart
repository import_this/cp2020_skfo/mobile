import 'dart:convert';

import 'package:latlong/latlong.dart';
import 'package:http/http.dart' as http;

class Address {
  /*
        "home_number": "56",
        "post_index": 355045,
        "street": 1,
        "city": 1,
        "state": 1,
        "number_of_floors": 9,
        "number_of_entrances": 4,
        "coords": {
            "latitude": 37.0625,
            "longitude": -95.677068
        },
        "building_type": 1,
        "building_status": 1,
        "photo": "https://api-post-info.midvikus.com/media/pv_918553.jpg"
  */
  //building_type: 1,
  //building_status: 1,
  final LatLng coords;

  String home_number;
  int building_type = 1;
  int building_status = 1;
  int number_of_floors = 1;
  int number_of_entrances = 1;

  Address({this.home_number, this.coords});

  static Future<List<Address>> geetList() async {
    var response = await http.get(
        'https://api-post-info.midvikus.com/api/addresses/',
        headers: {'Content-Type': 'application/json; charset=utf-8'});

    List<Address> data = [];
    for (var item in json.decode(utf8.decode(response.bodyBytes))) {
      data.add(Address(
          home_number: item["home_number"],
          coords:
              LatLng(item["coords"]["latitude"], item["coords"]["longitude"])));
    }
    return data;
  }

  Future<Address> save() async {
    var response =
        await http.post("https://api-post-info.midvikus.com/api/addresses/",
            headers: {'Content-Type': 'application/json; charset=utf-8'},
            body: json.encode({
              "home_number": this.home_number,
              "post_index": 355045,
              "street": 1,
              "city": 1,
              "state": 1,
              "number_of_floors": this.number_of_floors,
              "number_of_entrances": this.number_of_entrances,
              "coords": {
                "latitude": this.coords.latitude,
                "longitude": this.coords.longitude
              },
              "building_type": this.building_type,
              "building_status": this.building_status
            }));
    print(json.decode(utf8.decode(response.bodyBytes)));
  }
}

/*
Future<Map> ajaxGet(String serviceName) async {
  var responseBody = json.decode('{"data": "", "status": "404"}');
  try {
    var headers = {
          'X-DEVICE-ID': await _getDeviceIdentity(),
          'Authorization': 'JWT ' + await _getMobileToken(),
          'X-APP-ID': _applicationId,
          'Content-Type': 'application/json; charset=utf-8'
        };
    var response = await http.get(_urlBase + '/$serviceName',
        headers: headers);
    responseBody['data'] = json.decode(utf8.decode(response.bodyBytes));
    responseBody['status'] = response.statusCode ;
  } catch (e) {
    // An error was received
    throw new Exception("AJAX ERROR");
    
  }
  return responseBody;
}
*/
