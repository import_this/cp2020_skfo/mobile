import 'dart:convert';

import 'package:http/http.dart' as http;

class BuildingType {
  final int id;
  final String name;

  BuildingType(this.id, this.name);

  static Future<List<BuildingType>> geetList() async {
    var response = await http.get(
        'https://api-post-info.midvikus.com/api/building_types/',
        headers: {'Content-Type': 'application/json; charset=utf-8'});

    List<BuildingType> data = [];
    for (var item in json.decode(utf8.decode(response.bodyBytes))) {
      data.add(BuildingType(item["id"], item["name"]));
    }
    return data;
  }
}
