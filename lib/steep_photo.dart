import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:pochta/main.dart';
import 'package:pochta/steep_status.dart';
import 'package:pochta/widgets/button.dart';
import 'package:pochta/widgets/outline_button.dart';
import 'package:pochta/widgets/page_leading.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'models/address.dart';

class StepPhoto extends StatefulWidget {
  final Address address;

  const StepPhoto({Key key, this.address}) : super(key: key);

  @override
  _StepPhotoState createState() => _StepPhotoState();
}

class _StepPhotoState extends State<StepPhoto> {
  _saveMark() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        leading: IconButton(
          icon: Icon(Icons.close, color: Colors.black),
          onPressed: () => Navigator.of(context).pop(),
        ),
        backgroundColor: Color(0xFFFFFFFF),
        elevation: 0,
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              PageLeading(step: "Шаг 6", label: "Cделайте фотографию"),
              Padding(
                padding: EdgeInsets.only(bottom: 56),
              ),
              DottedBorder(
                  color: Color(0x12000000),
                  strokeWidth: 1,
                  dashPattern: [8, 8],
                  borderType: BorderType.RRect,
                  radius: Radius.circular(8),
                  child: Container(
                    padding: EdgeInsets.only(top: 40, bottom: 40),
                    color: Color(0x05000000),
                    child: Column(
                      children: [
                        Center(
                          child: Icon(
                            Icons.photo_camera,
                            color: Color(0x19000000),
                            size: 86,
                          ),
                        ),
                        Text(
                          "Снять",
                          style: TextStyle(fontSize: 22),
                        )
                      ],
                    ),
                  ))
            ],
          ),
        ),
      ),
      bottomNavigationBar: Padding(
          padding: EdgeInsets.all(12),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(right: 8.0),
                  child: PButtonOutline(
                    text: "Назад",
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(left: 8.0),
                  child: PButton(
                    text: "Далее",
                    onPressed: () async {
                      await widget.address.save();
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => MyHomePage()),
                      );
                    },
                  ),
                ),
              ),
            ],
          )),
    );
  }
}
