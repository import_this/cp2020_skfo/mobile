import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong/latlong.dart';
import 'package:pochta/models/address.dart';
import 'package:pochta/steap_addres.dart';
import 'package:pochta/widgets/button.dart';
import 'package:location/location.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
        // This makes the visual density adapt to the platform that you run
        // the app on. For desktop platforms, the controls will be smaller and
        // closer together (more dense) than on mobile platforms.
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  LocationData _currentLocation;
  MapController _mapController;

  bool _liveUpdate = true;
  bool _permission = false;

  String _serviceError = '';

  final Location _locationService = Location();

  List<Address> _marks = [];

  @override
  void initState() {
    super.initState();
    _mapController = MapController();
    initLocationService();
    _loadMarks();
  }

  _loadMarks() async {
    var marks = await Address.geetList();
    marks.cast();
    setState(() {
      _marks = marks;
    });
  }

  void initLocationService() async {
    await _locationService.changeSettings(
      accuracy: LocationAccuracy.high,
      interval: 1000,
    );

    LocationData location;
    bool serviceEnabled;
    bool serviceRequestResult;

    try {
      serviceEnabled = await _locationService.serviceEnabled();

      if (serviceEnabled) {
        var permission = await _locationService.requestPermission();
        _permission = permission == PermissionStatus.granted;

        if (_permission) {
          location = await _locationService.getLocation();
          _currentLocation = location;
          _locationService.onLocationChanged
              .listen((LocationData result) async {
            if (mounted) {
              setState(() {
                _currentLocation = result;

                // If Live Update is enabled, move map center
                if (_liveUpdate) {
                  _mapController.move(
                      LatLng(_currentLocation.latitude,
                          _currentLocation.longitude),
                      _mapController.zoom);
                }
              });
            }
          });
        }
      } else {
        serviceRequestResult = await _locationService.requestService();
        if (serviceRequestResult) {
          initLocationService();
          return;
        }
      }
    } on PlatformException catch (e) {
      print(e);
      if (e.code == 'PERMISSION_DENIED') {
        _serviceError = e.message;
      } else if (e.code == 'SERVICE_STATUS_ERROR') {
        _serviceError = e.message;
      }
      location = null;
    }
  }

  Color mainColor = Color(0xFF1A37FF);
  bool placeMark = false;

  void _incrementCounter() {
    setState(() {
      placeMark = true;
      _liveUpdate = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    LatLng currentLatLng;

    // Until currentLocation is initially updated, Widget can locate to 0, 0
    // by default or store previous location value to show.
    if (_currentLocation != null) {
      currentLatLng =
          LatLng(_currentLocation.latitude, _currentLocation.longitude);
    } else {
      currentLatLng = LatLng(0, 0);
    }

    var markers = <Marker>[
      Marker(
        width: 80.0,
        height: 80.0,
        point: currentLatLng,
        builder: (ctx) => Container(
          child: Icon(
            Icons.directions_run,
            color: Color(0xFF1A37FF),
          ),
        ),
      ),
      ..._marks.map((item) {
        return Marker(
          width: 80.0,
          height: 80.0,
          point: item.coords,
          builder: (ctx) => Container(
            child: Icon(
              Icons.place,
              color: Color(0xFF1A37FF),
              size: 45,
            ),
          ),
        );
      }).toList()
    ];
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updat_incrementCountering rather
    // than having to individually change instances of widgets.
    return Scaffold(
      //appBar: AppBar(
      // Here we take the value from the MyHomePage object that was created by
      // the App.build method, and use it to set our appbar title.
      //title: Text(widget.title),
      //),
      body: Stack(
        children: [
          new FlutterMap(
              mapController: _mapController,
              options: new MapOptions(
                center: new LatLng(45.02, 41.98),
                zoom: 16.0,
              ),
              layers: [
                new TileLayerOptions(
                    urlTemplate:
                        "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
                    subdomains: ['a', 'b', 'c']),
                MarkerLayerOptions(markers: markers)
              ]),
          Offstage(
              offstage: !this.placeMark,
              child: IconButton(
                alignment: Alignment(0, 5),
                icon: Icon(Icons.close, color: Colors.black),
                onPressed: () => {
                  setState(() {
                    placeMark = false;
                    _liveUpdate = true;
                  })
                },
              )),
          Offstage(
            offstage: !this.placeMark,
            child: Container(
                alignment: Alignment.center,
                child: IgnorePointer(
                  child: Icon(
                    Icons.location_on,
                    size: 90,
                  ),
                )),
          ),
          Offstage(
            offstage: !this.placeMark,
            child: Container(
                alignment: Alignment.bottomCenter,
                padding: EdgeInsets.fromLTRB(16, 0, 16, 16),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(bottom: 16),
                      child: Text(
                        "Поставить метку",
                        style: TextStyle(
                            fontSize: 24, fontWeight: FontWeight.w500),
                      ),
                    ),
                    SizedBox(
                      width: double.infinity,
                      child: PButton(
                        text: "Далее",
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => StepAddres(
                                    address: Address(
                                        coords: _mapController.center))),
                          );
                        },
                      ),
                    )
                  ],
                )),
          )
        ],
      ),
      floatingActionButton: this.placeMark
          ? null
          : FloatingActionButton(
              backgroundColor: Colors.white,
              onPressed: _incrementCounter,
              tooltip: 'Increment',
              child: Icon(
                Icons.add,
                color: this.mainColor,
                size: 40,
              ),
            ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
