import 'package:flutter/material.dart';
import 'package:pochta/steep_type.dart';
import 'package:pochta/widgets/button.dart';
import 'package:pochta/widgets/outline_button.dart';
import 'package:pochta/widgets/input.dart';

import 'models/address.dart';

class StepAddres extends StatefulWidget {
  final Address address;

  const StepAddres({Key key, this.address}) : super(key: key);

  @override
  _StepAddresState createState() => _StepAddresState();
}

class _StepAddresState extends State<StepAddres> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        leading: IconButton(
          icon: Icon(Icons.close, color: Colors.black),
          onPressed: () => Navigator.of(context).pop(),
        ),
        backgroundColor: Color(0xFFFFFFFF),
        elevation: 0,
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                "Шаг 1",
                style: TextStyle(
                    color: Color(0x7F000000),
                    fontWeight: FontWeight.w500,
                    fontSize: 18),
              ),
              Text("Введите адрес",
                  style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.w500,
                      fontSize: 28)),
              CustomInput(label: "Улица", onChanged: (value) => {}),
              CustomInput(
                  label: "Дом",
                  onChanged: (value) => {widget.address.home_number = value}),
            ],
          ),
        ),
      ),
      bottomNavigationBar: Padding(
          padding: EdgeInsets.all(12),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(right: 8.0),
                  child: PButtonOutline(
                    text: "Назад",
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(left: 8.0),
                  child: PButton(
                    text: "Далее",
                    onPressed: () => {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                StepType(address: widget.address)),
                      )
                    },
                  ),
                ),
              ),
            ],
          )),
    );
  }
}
