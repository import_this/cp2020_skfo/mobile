import 'package:flutter/material.dart';
import 'package:pochta/steep_entrance.dart';
import 'package:pochta/widgets/button.dart';
import 'package:pochta/widgets/outline_button.dart';
import 'package:pochta/widgets/input.dart';
import 'package:pochta/widgets/page_leading.dart';

import 'models/address.dart';

class StepFloor extends StatefulWidget {
  final Address address;

  const StepFloor({Key key, this.address}) : super(key: key);

  @override
  _StepFloorState createState() => _StepFloorState();
}

class _StepFloorState extends State<StepFloor> {
  List<Map> selectValues = List<Map>.generate(20,
      (int index) => {"value": (index + 1), "text": (index + 1).toString()});

  @override
  Widget build(BuildContext context) {
    var markers = selectValues.map((item) {
      return Row(
        children: [
          Radio(
            value: item["value"],
            groupValue: widget.address.number_of_floors,
            onChanged: (value) {
              setState(() {
                widget.address.number_of_floors = value;
              });
            },
          ),
          Text(
            item["text"],
            style: TextStyle(fontSize: 22),
          ),
        ],
      );
    }).toList();

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        leading: IconButton(
          icon: Icon(Icons.close, color: Colors.black),
          onPressed: () => Navigator.of(context).pop(),
        ),
        backgroundColor: Color(0xFFFFFFFF),
        elevation: 0,
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              PageLeading(step: "Шаг 4", label: "Сколько этажей"),
              Padding(
                padding: EdgeInsets.only(bottom: 56),
              ),
              ...markers
            ],
          ),
        ),
      ),
      bottomNavigationBar: Padding(
          padding: EdgeInsets.all(12),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(right: 8.0),
                  child: PButtonOutline(
                    text: "Назад",
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(left: 8.0),
                  child: PButton(
                    text: "Далее",
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => StepEntrance(
                                  address: widget.address,
                                )),
                      );
                    },
                  ),
                ),
              ),
            ],
          )),
    );
  }
}
